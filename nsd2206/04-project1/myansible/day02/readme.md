# 文件说明
[toc]
- `ansible.cfg`：主配置文件
- `inventory`：主机清单文件
- `files`目录：用于保存需要上传的文件
- `roles`目录：角色目录
- `roles/inst_pkg`目录：用于装包的角色
- `roles/enable_svr`目录：用于起服务的角色
- `scripts`目录：用于保存在服务器上运行的脚本
- `01_upload_repo.yml`：配置yum
- `02_get_nginx.yml`：将web1的nginx打包下载
- `03_deploy_nginx.yml`：解压下载的nginx到web2和web3
- `04_config_ngx_svr.yml`：启动nginx服务
- `05_config_php.yml`：配置php服务
- `06_upload_repo_nfs.yml`：配置nfs的yum
- `07_inst_nfs.yml`：安装nfs
- `08_config_nfs.yml`：创建nfs共享
- `09_enable_nfs.yml`：启动nfs服务
- `10_get_html.yml`：打包并下载nginx的html目录
- `11_upload_nfs.yml`：解压下载的html目录到nfs共享
- `12_rm_html.yml`：删除web服务器上的html目录
- `13_mount_html.yml`：挂载nfs共享到web服务器
- `14_upload_repo_proxy.yml`：配置lbservers的yum
- `15_inst_haproxy.yml`：安装haproxy
- `16_config_haproxy.yml`：配置并启动haproxy
