# day02

[toc]

## KeepAlived高可用集群

- keepalived用于实现高可用集群
- 它的工作原理就是VRRP（虚拟冗余路由协议）

### 配置高可用的web集群

![image-20211103094124024](../imgs/image-20211103094124024.png)

- 环境说明：
  - web1：eth0->192.168.4.100/24
  - web2：eth0->192.168.4.200/24

- 配置keepalived

```shell
# 在两台web服务器上安装keepalived
[root@web1 ~]# yum install -y keepalived httpd
[root@web2 ~]# yum install -y keepalived httpd

# 修改配置文件
[root@web1 ~]# vim /etc/keepalived/keepalived.conf 
 12    router_id web1    # 设置本机在集群中的唯一识别符
 13    vrrp_iptables     # 自动配置iptables放行规则
 ... ...
 20 vrrp_instance VI_1 {
 21     state MASTER           # 状态，主为MASTER，备为BACKUP
 22     interface eth0         # 网卡
 23     virtual_router_id 51   # 虚拟路由器地址
 24     priority 100           # 优先级
 25     advert_int 1           # 发送心跳消息的间隔
 26     authentication {
 27         auth_type PASS     # 认证类型为共享密码
 28         auth_pass 1111     # 集群中的机器密码相同，才能成为集群
 29     }   
 30     virtual_ipaddress {
 31         192.168.4.80/24    # VIP地址
 32     }   
 33 }
# 删除下面所有行

# 打开一个新的终端监控日志，新日志将出持续显示在屏幕上。退出按ctrl+c
[root@web1 ~]# tail -f /var/log/messages | grep -i keepalived
# 在前一个终端启动服务
[root@web1 ~]# systemctl start keepalived
# 等几秒服务完全启动后，可以查看到vip
[root@web1 ~]# ip a s eth0   # eth0将会增加额外的4.80地址


# 配置web2
[root@web1 ~]# scp /etc/keepalived/keepalived.conf 192.168.4.200:/etc/keepalived/
[root@web2 ~]# vim /etc/keepalived/keepalived.conf 
 12    router_id web2          # 改id
 13    vrrp_iptables
 ... ... 
 20 vrrp_instance VI_1 {
 21     state BACKUP           # 改状态
 22     interface eth0
 23     virtual_router_id 51
 24     priority 80            # 改优先级
 25     advert_int 1
 26     authentication {
 27         auth_type PASS
 28         auth_pass 1111
 29     }
 30     virtual_ipaddress {
 31         192.168.4.80/24
 32     }
 33 }

# 启动服务
[root@web2 ~]# systemctl start keepalived
# 查看地址，eth0不会出现vip
[root@web2 ~]# ip a s eth0


# 测试，现在访问4.80，看到是web1上的内容
[root@client1 ~]# curl http://192.168.4.80/
192.168.2.100

# 在web2上监控日志
[root@web2 ~]# tail -f /var/log/messages | grep -i keepalived
# 关闭web1上的keepalived，观察web2的日志输出
[root@web1 ~]# systemctl stop keepalived.service 

# 测试，现在访问4.80，看到是web2上的内容
[root@client1 ~]# curl http://192.168.4.80/
apache web server2

# 在web2上查看vip，可以查看到vip 192.168.4.80
[root@web2 ~]# ip a s eth0
```

### 配置高可用、负载均衡的web集群

![image-20211103113155142](../imgs/image-20211103113155142.png)

- 环境说明：LVS-DR模式
  - client1：eth0->192.168.4.10
  - lvs1：eth0->192.168.4.5
  - lvs2：eth0->192.168.4.6
  - web1：eth0->192.168.4.100
  - web2：eth0->192.168.4.200

- 环境准备

```shell
# 关闭2台web服务器上的keepalived
[root@web1 ~]# systemctl stop keepalived.service 
[root@web2 ~]# systemctl stop keepalived.service 
[root@web1 ~]# yum remove -y keepalived
[root@web2 ~]# yum remove -y keepalived

# 创建新虚拟机lvs2
[root@zzgrhel8 ~]# clone-vm7
Enter VM number: 5    # 虚拟机编号，
[root@zzgrhel8 ~]# virsh start tedu_node05     # 启动虚机
[root@zzgrhel8 ~]# virsh console tedu_node05   # 连接虚机控制台
localhost login: root
Password: 123456
# 将以下内容粘贴到虚拟机，进行初始化
hostnamectl set-hostname lvs2
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.6/24
nmcli connection down eth0
nmcli connection up eth0
echo a | passwd --stdin root

# 退出
[root@localhost ~]# exit
# 按ctrl + ]退回到真机

# 连接测试
[root@zzgrhel8 ~]# ssh 192.168.4.6
```

#### 配置高可用、负载均衡

1. 在2台web服务器的lo上配置vip

2. 在2台web服务器上配置内核参数
3. 删除lvs1上的`eth0:0`。因为vip将由keepalived接管

```shell
[root@lvs1 ~]# ifdown eth0:0
[root@lvs1 ~]# rm -f /etc/sysconfig/network-scripts/ifcfg-eth0:0
```

4. 删除lvs1上的lvs规则。因为lvs规则将由keepalived创建

```shell
[root@lvs1 ~]# ipvsadm -Ln   # 查看规则
[root@lvs1 ~]# ipvsadm -D -t 192.168.4.15:80
```

5. 在lvs上配置keepalived

```shell
[root@lvs1 ~]# yum install -y ipvsadm keepalived
[root@lvs2 ~]# yum install -y ipvsadm keepalived

[root@lvs1 ~]# vim /etc/keepalived/keepalived.conf 
 12    router_id lvs1       # 为本机取一个唯一的id
 13    vrrp_iptables        # 自动开启iptables放行规则
... ...
 20 vrrp_instance VI_1 {
 21     state MASTER
 22     interface eth0
 23     virtual_router_id 51
 24     priority 100
 25     advert_int 1
 26     authentication {
 27         auth_type PASS
 28         auth_pass 1111
 29     }   
 30     virtual_ipaddress {
 31         192.168.4.15       # vip地址，与web服务器的vip一致
 32     }   
 33 }
 # 以下为keepalived配置lvs的规则
 35 virtual_server 192.168.4.15 80 {   # 声明虚拟服务器地址
 36     delay_loop 6     # 健康检查延迟6秒开始
 37     lb_algo wrr      # 调度算法为wrr
 38     lb_kind DR       # 工作模式为DR
 39     persistence_timeout 50  # 50秒内相同客户端调度到相同服务器
 40     protocol TCP     # 协议是TCP
 41 
 42     real_server 192.168.4.100 80 {   # 声明真实服务器
 43         weight 1          # 权重
 44         TCP_CHECK {       # 通过TCP协议对真实服务器做健康检查
 45             connect_timeout 3 # 连接超时时间为3秒
 46             nb_get_retry 3    # 3次访问失败则认为真实服务器故障
 47             delay_before_retry 3  # 两次检查时间的间隔3秒
 48         }
 49     }
 50     real_server 192.168.4.200 80 {
 51         weight 2
 52         TCP_CHECK {
 53             connect_timeout 3
 54             nb_get_retry 3
 55             delay_before_retry 3
 56         }
 57     }
 58 }
# 以下部分删除

# 启动keepalived服务
[root@lvs1 ~]# systemctl start keepalived

# 验证
[root@lvs1 ~]# ip a s eth0    # 可以查看到vip
[root@lvs1 ~]# ipvsadm -Ln    # 出现规则


# 客户端连接测试
[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.15/; done
apache web server2
apache web server2
apache web server2
apache web server2
apache web server2
apache web server2
# 为了效率相同的客户端在50秒内分发给同一台服务器。为了使用同一个客户端可以看到轮询效果，可以注释配置文件中相应的行后，重启keepavlied。


# 配置LVS2
[root@lvs1 ~]# scp /etc/keepalived/keepalived.conf 192.168.4.6:/etc/keepalived/
[root@lvs2 ~]# vim /etc/keepalived/keepalived.conf 
 12    router_id lvs2
 21     state BACKUP
 24     priority 80
[root@lvs2 ~]# systemctl start keepalived
[root@lvs2 ~]# ipvsadm -Ln   # 出现规则
```

6. 验证

```shell
# 1. 验证真实服务器健康检查
[root@web1 ~]# systemctl stop httpd
[root@lvs1 ~]# ipvsadm -Ln   # web1在规则中消失
[root@lvs2 ~]# ipvsadm -Ln

[root@web1 ~]# systemctl start httpd
[root@lvs1 ~]# ipvsadm -Ln   # web1重新出现在规则中
[root@lvs2 ~]# ipvsadm -Ln

# 2. 验证lvs的高可用性
[root@lvs1 ~]# shutdown -h now    # 关机
[root@lvs2 ~]# ip a s eth0     # 可以查看到vip
# 客户端访问vip依然可用
[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.15/; done
192.168.2.100
apache web server2
apache web server2
192.168.2.100
apache web server2
apache web server2

```

## HAProxy

- 也是一款实现负载均衡的调度器
- 适用于负载特别大的web站点 
- HAProxy的工作模式：
  - mode http：只适用于web服务
  - mode tcp：适用于各种服务
  - mode health：仅做健康检查，很少使用

### 配置haproxy

![image-20211103162509637](../imgs/image-20211103162509637.png)

- 环境准备：
  - client1：eth0 -> 192.168.4.10
  - HAProxy：eth0 -> 192.168.4.5
  - web1：eth0 -> 192.168.4.100
  - web2：eth0 -> 192.168.4.200

- 初始化配置

```shell
# 关闭192.168.4.6
[root@lvs2 ~]# shutdown -h now

# 清理192.168.4.5
[root@lvs1 ~]# yum remove -y ipvsadm keepalived
[root@lvs1 ~]# hostnamectl set-hostname haproxy1

# web服务器，不需要配置vip，不需要改内核参数。但是存在对haproxy也没有影响。
```

- 配置haproxy

```shell
# 装包
[root@haproxy1 ~]# yum install -y haproxy
[root@haproxy1 ~]# vim /etc/haproxy/haproxy.cfg 
# 配置文件中，global是全局配置；default是缺省配置，如果后续有和default相同的配置，default配置将会被覆盖。
# 配置文件中，frontend描述haproxy怎么和用户交互；backend描述haproxy怎么和后台应用服务器交互。这两个选项，一般不单独使用，而是合并到一起，名为listen。
# 将61行之后全部删除，写入以下内容
 61 listen myweb 0.0.0.0:80   # 定义本机监听地址
 62     balance roundrobin    # 调度算法为轮询
 # 对web服务器做健康检查，2秒检查一次，如果连续2次检查成功，认为服务器是健康的，如果连续5次检查失败，认为服务器坏了
 63     server web1 192.168.4.100 check inter 2000 rise 2 fall 5
 64     server web2 192.168.4.200 check inter 2000 rise 2 fall 5
 65     
 66 listen stats 0.0.0.0:1080  # 定义监控地址
 67     stats refresh 30s      # 设置监控页面自动刷新时间为30秒
 68     stats uri /stats       # 定义监控地址是/stats
 69     stats auth admin:admin  # 监控页面的用户名和密码都是admin

# 启服务
[root@haproxy1 ~]# systemctl start haproxy.service 
# 使用firefox访问监控地址 http://192.168.4.5:1080/stats

# 客户端访问测试
[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.5/; done
192.168.2.100
apache web server2
192.168.2.100
apache web server2
192.168.2.100
apache web server2

```

明天用到的虚拟机：192.168.4.11-192.168.4.13，名字是node1-node3。client1，ip是192.168.4.10。配置好yum，关闭防火墙和selinux。

