# day04

[toc]

## 自定义错误页面

```shell
# 存放一张名为err.png的图片到/usr/local/nginx/html/wp-content
# 创建404.html
[root@web1 html]# vim /usr/local/nginx/html/404.html 
<img src="wp-content/err.png">
# 修改配置文件
[root@web1 html]# vim /usr/local/nginx/conf/nginx.conf
        error_page  404              /404.html;
[root@web2 html]# vim /usr/local/nginx/conf/nginx.conf
        error_page  404              /404.html;
[root@web3 html]# vim /usr/local/nginx/conf/nginx.conf
        error_page  404              /404.html;
[root@web1 html]# systemctl restart nginx.service 
[root@web2 html]# systemctl restart nginx.service 
[root@web3 html]# systemctl restart nginx.service 
```

- 访问不存在的路径，将会出现自定义错误页面

## 升级nginx

参考：https://www.jianshu.com/p/9fc4aa46f423

### 升级流程

1. 在haproxy中注释web1的地址
2. 升级web1
3. 测试web1
4. 在haproxy中添加web1的地址，注释掉web2和web3
5. 升级web2和web3并测试
6. web2和web3测试成功后，在haproxy中添加web2和web3

### 实施

```shell
# 在haproxy中注释web1的地址
[root@proxy ~]# vim /etc/haproxy/haproxy.cfg 
... ...
listen wordpress *:80
    balance roundrobin
    # server web1 192.168.2.11:80 check inter 2000 rise 2 fall 3
    server web2 192.168.2.12:80 check inter 2000 rise 2 fall 3
    server web3 192.168.2.13:80 check inter 2000 rise 2 fall 3
... ...
[root@proxy ~]# systemctl restart haproxy

# 升级web1
[root@web1 ~]# cd lnmp_soft/
[root@web1 lnmp_soft]# tar xf nginx-1.15.8.tar.gz 
[root@web1 lnmp_soft]# cd nginx-1.15.8/
[root@web1 nginx-1.15.8]# ./configure --with-http_ssl_module --with-http_stub_status_module
[root@web1 nginx-1.15.8]# make
[root@web1 nginx-1.15.8]# mv /usr/local/nginx/sbin/nginx ~/nginx.old
[root@web1 nginx-1.15.8]# cp objs/nginx /usr/local/nginx/sbin/
[root@web1 nginx-1.15.8]# systemctl restart nginx.service 
[root@web1 nginx-1.15.8]# ss -tlnp | grep :80
LISTEN     0      128          *:80
# 访问：http://192.168.2.11/测试是否正常

# 在haproxy中注释web2和web3，添加web1
[root@proxy ~]# vim /etc/haproxy/haproxy.cfg 
... ...
listen wordpress *:80
    balance roundrobin
    server web1 192.168.2.11:80 check inter 2000 rise 2 fall 3
    # server web2 192.168.2.12:80 check inter 2000 rise 2 fall 3
    # server web3 192.168.2.13:80 check inter 2000 rise 2 fall 3
... ...
[root@proxy ~]# systemctl restart haproxy

# 升级web2和web3
[root@web2 ~]# systemctl stop nginx.service 
[root@web2 ~]# mv /usr/local/nginx/sbin/nginx ~/nginx.old
[root@web3 ~]# systemctl stop nginx.service 
[root@web3 ~]# mv /usr/local/nginx/sbin/nginx ~/nginx.old
[root@web1 ~]# scp /usr/local/nginx/sbin/nginx 192.168.2.12:/usr/local/nginx/sbin/
[root@web1 ~]# ^12^13
[root@web2 ~]# systemctl start nginx.service 
[root@web3 ~]# systemctl start nginx.service 
# 访问：http://192.168.2.12/测试是否正常
# 访问：http://192.168.2.13/测试是否正常

# 在haproxy中，添加web2和web3
[root@proxy ~]# vim /etc/haproxy/haproxy.cfg 
... ...
listen wordpress *:80
    balance roundrobin
    server web1 192.168.2.11:80 check inter 2000 rise 2 fall 3
    server web2 192.168.2.12:80 check inter 2000 rise 2 fall 3
    server web3 192.168.2.13:80 check inter 2000 rise 2 fall 3
... ...
[root@proxy ~]# systemctl restart haproxy
```

## 生产环境下的yum

### 国内开源镜像站点

- http://mirrors.163.com/
- https://mirrors.tencent.com/
- https://mirrors.sohu.com/
- https://developer.aliyun.com/mirror/

### 使用在线yum源

- 启动一台虚拟机，将第一块网卡连到default网络（vmware虚拟机连接vmnet8）
- ip地址选择自动获取，虚拟机即可接入互联网
- centos常用的yum源有系统yum和epel
  - epel：企业版Linux额外的软件包

```shell
# 配置centos7使用阿里云centos yum源安装python3
[root@localhost ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
[root@localhost ~]# yum install python3

# 配置centos7使用阿里云epel源安装nginx
[root@localhost ~]# wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
[root@localhost ~]# yum install nginx
[root@localhost ~]# systemctl start nginx

# 安装小火车
[root@localhost ~]# yum install sl
[root@localhost ~]# sl

# 安装ansible
[root@localhost ~]# yum install ansible
```











