# 文件说明
[toc]
- `ansible.cfg`：主配置文件
- `inventory`：主机清单文件
- `files`目录：用于保存需要上传的文件
- `roles`目录：角色目录
- `roles/inst_pkg`目录：用于装包的角色
- `roles/enable_svr`目录：用于起服务的角色
- `scripts`目录：用于保存在服务器上运行的脚本
- `01_config_sshkey.yml`：分发密钥到各节点，执行时使用asible-playbook 01_config_sshkey.yml -k
- `02_upload_repo_proxy.yml`：配置yum
- `03_config_hosts.yml`：配置/etc/hosts文件
- `04_inst_ceph.yml`：安装ceph相关软件包
- `05_inst_chrony.yml`：安装chrony包
- `06_config_chrony_server.yml`：配置node1为chronyd服务器
- `07_config_chrony_client.yml`：配置node2和node3为chronyd客户端
- `08_inst_cephtool.yml`：安装ceph-deploy工具
