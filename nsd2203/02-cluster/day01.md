# day01

[toc]

## 集群

- 将很多机器组织到一起，作为一个整体对外提供服务
- 集群在扩展性、性能方面都可以做到很灵活
- 集群分类：
  - 负载均衡集群：Load Balance
  - 高可用集群：High Availability
  - 高性能计算：High Performance Computing

## LVS

- LVS：Linux Virtual Server，Linux虚拟服务器
- 实现负载均衡集群
- 作者：章文嵩。国防科技大学读博士期间编写
- LVS的工作模式：
  - NAT：网络地址转换
  - DR：路由模式
  - TUN：隧道模式
- 术语：
  - 调度器：LVS服务器
  - 真实服务器Real Server：提供服务的服务器
  - VIP：虚拟地址，提供给用户访问的地址
  - DIP：指定地址，LVS服务器上与真实服务器通信的地址
  - RIP：真实地址，真实服务器的地址

- 常见的调度算法，共10个，常用的有4个：
  - 轮询rr：Real Server轮流提供服务
  - 加权轮询wrr：根据服务器性能设置权重，权重大的得到的请求更多
  - 最少连接lc：根据Real Server的连接数分配请求
  - 加权最少连接wlc：类似于wrr，根据权重分配请求

### 配置LVS NAT模式

![image-20211102101942994](../imgs/image-20211102101942994.png)

- 环境准备
  - client1：eth0->192.168.4.10，网关192.168.4.5
  - lvs1: eth0 -> 192.168.4.5；eth1->192.168.2.5
  - web1：eth1->192.168.2.100；网关192.168.2.5
  - web2：eth1->192.168.2.200；网关192.168.2.5

```shell
# 创建4台虚拟机
[root@zzgrhel8 ~]# clone-vm7 
Enter VM number: 1    # 此处填的数字，是虚拟机编号
[root@zzgrhel8 ~]# clone-vm7 
Enter VM number: 2
[root@zzgrhel8 ~]# clone-vm7 
Enter VM number: 3
[root@zzgrhel8 ~]# clone-vm7 
Enter VM number: 4

# 查看虚拟机
[root@zzgrhel8 ~]# virsh list --all
 Id   名称          状态
--------------------------
 -    tedu_node01   关闭
 -    tedu_node02   关闭
 -    tedu_node03   关闭
 -    tedu_node04   关闭

# 启动虚拟机
[root@zzgrhel8 ~]# for i in {1..4}
> do
> virsh start tedu_node0$i
> done

# 初始化虚拟机
[root@zzgrhel8 ~]# virsh console tedu_node01  # 连接tedu_node01控制台
localhost login: root
Password: 123456
# 登陆之后，将以下内容粘贴到终端
hostnamectl set-hostname client1
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.10/24
nmcli connection down eth0
nmcli connection up eth0
echo a | passwd --stdin root
nmcli connection modify eth0 ipv4.gateway 192.168.4.5
nmcli connection down eth0
nmcli connection up eth0

# 退出
[root@localhost ~]# exit
# 退出之后，按ctrl+]可回到真机

# 真机通过ssh连接client1
[root@zzgrhel8 ~]# rm -f ~/.ssh/known_hosts 
[root@zzgrhel8 ~]# ssh 192.168.4.10


# 配置第2台机器作为lvs1
[root@zzgrhel8 ~]# virsh console tedu_node02
Kernel 3.10.0-862.el7.x86_64 on an x86_64

localhost login: root
Password: 123456

# 登陆之后，将以下内容复制到命令行
hostnamectl set-hostname lvs1
nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.5/24
nmcli connection down eth0
nmcli connection up eth0
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.5/24
nmcli connection down eth1
nmcli connection up eth1
echo a | passwd --stdin root

# 退出
[root@localhost ~]# exit
# 退出之后，按ctrl+]可回到真机

# 真机通过ssh连接lvs1
[root@zzgrhel8 ~]# ssh 192.168.4.5



# 配置第3台机器作为web1
[root@zzgrhel8 ~]# virsh console tedu_node03
localhost login: root
Password: 123456

# 登陆之后，将以下内容复制到命令行
hostnamectl set-hostname web1
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.100/24
nmcli connection down eth1
nmcli connection up eth1
nmcli connection modify eth1 ipv4.gateway 192.168.2.5
nmcli connection down eth1
nmcli connection up eth1
echo a | passwd --stdin root

# 退出
[root@localhost ~]# exit
# 退出之后，按ctrl+]可回到真机

# 真机通过ssh连接web1
[root@zzgrhel8 ~]# ssh 192.168.2.100




# 配置第4台机器作为web2
[root@zzgrhel8 ~]# virsh console tedu_node04
localhost login: root
Password: 123456

# 登陆之后，将以下内容复制到命令行
hostnamectl set-hostname web2
nmcli connection modify eth1 ipv4.method manual ipv4.addresses 192.168.2.200/24
nmcli connection down eth1
nmcli connection up eth1
nmcli connection modify eth1 ipv4.gateway 192.168.2.5
nmcli connection down eth1
nmcli connection up eth1
echo a | passwd --stdin root

# 退出
[root@localhost ~]# exit
# 退出之后，按ctrl+]可回到真机

# 真机通过ssh连接web2
[root@zzgrhel8 ~]# ssh 192.168.2.200
```

- 通过clone-vm7创建出来的虚拟机器，如果有192.168.4.0网段的地址，yum已经配置了。虚拟机已关闭selinux和防火墙 。

#### 配置LVS NAT模式步骤

- 配置2台web服务器

```shell
[root@web1 ~]# vim /etc/yum.repos.d/local.repo 
[local_repo]
name=CentOS-$releasever - Base
baseurl=ftp://192.168.2.254/centos-1804
enabled=1
gpgcheck=0
[root@web2 ~]# vim /etc/yum.repos.d/local.repo 
[local_repo]
name=CentOS-$releasever - Base
baseurl=ftp://192.168.2.254/centos-1804
enabled=1
gpgcheck=0
[root@web1 ~]# yum install -y httpd
[root@web2 ~]# yum install -y httpd

# 创建测试页面
[root@web1 ~]# echo "192.168.2.100" > /var/www/html/index.html
[root@web2 ~]# echo "apache web server2" > /var/www/html/index.html
[root@web1 ~]# systemctl enable httpd --now
[root@web2 ~]# systemctl enable httpd --now

# 在lvs1上测试到web服务器的访问
[root@lvs1 ~]# curl http://192.168.2.100/
192.168.2.100
[root@lvs1 ~]# ^100^200   # 将上一条命令中的100换成200，执行
curl http://192.168.2.200/
apache web server2
```

- 确保lvs1的ip转发功能已经打开。该功能需要改变内核参数

```shell
# 查看ip转发功能的内核参数
[root@lvs1 ~]# sysctl -a    # 查看所有的内核参数
[root@lvs1 ~]# sysctl -a | grep ip_forward  # 查看ip_foward参数
net.ipv4.ip_forward = 1   # 1表示打开转发，0表示关闭转发

# 永久设置打开ip_forward功能
[root@lvs1 ~]# echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf 
[root@lvs1 ~]# sysctl -p

# 测试从客户端到服务器的访问
[root@client1 ~]# curl http://192.168.2.100/
192.168.2.100
[root@client1 ~]# curl http://192.168.2.200/
apache web server2
```

- 安装LVS

```shell
[root@lvs1 ~]# yum install -y ipvsadm
```

- ipvsadm使用说明

```shell
[root@lvs1 ~]# ipvsadm
-A: 添加虚拟服务器
-E: 编辑虚拟服务器
-D: 删除虚拟服务器
-t: 添加tcp服务器
-u: 添加udp服务器
-s: 指定调度算法。如轮询rr/加权轮询wrr/最少连接lc/加权最少连接wlc

-a: 添加虚拟服务器后，向虚拟服务器中加入真实服务器
-r: 指定真实服务器
-w: 设置权重
-m: 指定工作模式为NAT
-g: 指定工作模式为DR
```

- 配置LVS

```shell
# 为web服务器创建虚拟服务器，使用rr调度算法
[root@lvs1 ~]# ipvsadm -A -t 192.168.4.5:80 -s rr
# 查看配置
[root@lvs1 ~]# ipvsadm -Ln  # L是列出，n是使用数字，而不是名字

# 向虚拟服务器中添加RIP
[root@lvs1 ~]# ipvsadm -a -t 192.168.4.5:80 -r 192.168.2.100 -w 1 -m
[root@lvs1 ~]# ipvsadm -a -t 192.168.4.5:80 -r 192.168.2.200 -w 2 -m
# 查看配置
[root@lvs1 ~]# ipvsadm -Ln

# 验证
[root@client1 ~]# for i in {1..4}
> do
> curl http://192.168.4.5/
> done
apache web server2
192.168.2.100
apache web server2
192.168.2.100

# 删除配置。（如果配置有错，用以下命令删除重配置）
[root@lvs1 ~]# ipvsadm -D -t 192.168.4.5:80


# 修改调度模式为加权轮询
[root@lvs1 ~]# ipvsadm -E -t 192.168.4.5:80 -s wrr
# 验证配置
[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.5/; done
apache web server2
apache web server2
192.168.2.100
apache web server2
apache web server2
192.168.2.100
```

## LVS DR模式

![image-20211102151338519](../imgs/image-20211102151338519.png)

- LVS DR模式，LVS主机和web服务器都是单网卡。它们连在同一网络中
- 修改实验环境
  - client1：eth0-> 192.168.4.10
  - lvs1：eth0->192.168.4.5，删除eth1的IP
  - web1：eth0->192.168.4.100，删除eth1的IP
  - web2：eth0->192.168.4.200，删除eth1的IP

```shell
# 删除lvs虚拟服务器配置
[root@lvs1 ~]# ipvsadm -D -t 192.168.4.5:80
[root@lvs1 ~]# ipvsadm -Ln

# 删除lvs1上eth1的配置
[root@lvs1 ~]# nmcli connection modify eth1 ipv4.method disabled ipv4.addresses ''
[root@lvs1 ~]# ifdown eth1


# 修改web1的配置：停掉eth1的地址。配置eth0的地址为192.168.4.100
# 进入网卡配置文件目录
[root@web1 ~]# cd /etc/sysconfig/network-scripts/
# eth0网卡的配置文件叫ifcfg-eth0
[root@web1 network-scripts]# ls ifcfg-eth*
ifcfg-eth0  ifcfg-eth1	ifcfg-eth2  ifcfg-eth3
# 配置eth0地址
[root@web1 network-scripts]# vim ifcfg-eth0
TYPE=Ethernet             # 网络类型为以太网
BOOTPROTO=none            # IP地址是静态配置的，也可以用static
NAME=eth0                 # 为设备重命名
DEVICE=eth0               # 网卡设备名
ONBOOT=yes                # 开机激活网卡
IPADDR=192.168.4.100      # IP地址
NETMASK=255.255.255.0     # 子网掩码
GATEWAY=192.168.4.254     # 网关
[root@web1 network-scripts]# ifdown eth0; ifup eth0  # 禁用激活网卡

# 在web1上停掉eth1
[root@web1 ~]# vim /etc/sysconfig/network-scripts/ifcfg-eth1
TYPE=Ethernet
BOOTPROTO=none
NAME=eth1
DEVICE=eth1
ONBOOT=no
[root@web1 ~]# ifdown eth1


# 修改web2的网络
[root@web2 ~]# vim /etc/sysconfig/network-scripts/ifcfg-eth0 
TYPE=Ethernet
BOOTPROTO=none
NAME=eth0
DEVICE=eth0
ONBOOT=yes
IPADDR=192.168.4.200
NETMASK=255.255.255.0
GATEWAY=192.168.4.254
[root@web2 ~]# ifdown eth0; ifup eth0
[root@web2 ~]# vim /etc/sysconfig/network-scripts/ifcfg-eth1
TYPE=Ethernet
BOOTPROTO=none
NAME=eth1
DEVICE=eth1
ONBOOT=no
[root@web2 ~]# ifdown eth1   # 终端卡住，关掉它，在新终端重新连
```

### 配置LVS DR模式

1. 在lvs1的eth0上配置vip 192.168.4.15。通过为eth0创建逻辑端口的方式配置vip，为逻辑端口起名为eth0:0

```shell
[root@lvs1 ~]# cd /etc/sysconfig/network-scripts/
[root@lvs1 network-scripts]# cp ifcfg-eth0 ifcfg-eth0:0
[root@lvs1 network-scripts]# vim ifcfg-eth0:0
TYPE=Ethernet
BOOTPROTO=none
NAME=eth0:0
DEVICE=eth0:0
ONBOOT=yes
IPADDR=192.168.4.15
PREFIX=24
[root@lvs1 network-scripts]# ifup eth0:0
# 查看新的地址
[root@lvs1 network-scripts]# ifconfig 
```

2. 在2台web服务器的lo上配置vip 192.168.4.15

```shell
[root@web1 ~]# cd /etc/sysconfig/network-scripts/
[root@web1 network-scripts]# cp ifcfg-lo ifcfg-lo:0
[root@web1 network-scripts]# vim ifcfg-lo:0
DEVICE=lo:0
IPADDR=192.168.4.15
NETMASK=255.255.255.255
NETWORK=192.168.4.15
BROADCAST=192.168.4.15
ONBOOT=yes
NAME=lo:0
[root@web1 network-scripts]# ifup lo:0
[root@web1 network-scripts]# ifconfig 
# 把web1的配置拷贝到web2上
[root@web1 network-scripts]# scp ./ifcfg-lo:0 192.168.4.200:/etc/sysconfig/network-scripts/
[root@web2 ~]# ifup lo:0
[root@web2 ~]# ifconfig 
```

3. 在2台web服务器上配置内核参数，使得它们不响应对192.168.4.15的请求

```shell
[root@web1 ~]# sysctl -a | grep arp_ignore
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.lo.arp_ignore = 0
[root@web1 ~]# sysctl -a | grep arp_announce
net.ipv4.conf.all.arp_announce = 2
net.ipv4.conf.lo.arp_announce = 0

[root@web1 ~]# vim /etc/sysctl.conf 
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.lo.arp_ignore = 1
net.ipv4.conf.all.arp_announce = 2
net.ipv4.conf.lo.arp_announce = 2
[root@web1 ~]# sysctl -p

[root@web2 ~]# vim /etc/sysctl.conf 
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.lo.arp_ignore = 1
net.ipv4.conf.all.arp_announce = 2
net.ipv4.conf.lo.arp_announce = 2
[root@web2 ~]# sysctl -p
```

4. 在lvs1上配置虚拟服务器

```shell
# 创建虚拟服务器
[root@lvs1 ~]# ipvsadm -A -t 192.168.4.15:80 -s wlc
# 向虚拟服务器中加真实服务器
[root@lvs1 ~]# ipvsadm -a -t 192.168.4.15:80 -r 192.168.4.100 -w 1 -g
[root@lvs1 ~]# ipvsadm -a -t 192.168.4.15:80 -r 192.168.4.200 -w 2 -g
# 查看配置
[root@lvs1 ~]# ipvsadm -Ln

# 客户验证
[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.15/; done
apache web server2
192.168.2.100
apache web server2
apache web server2
192.168.2.100
apache web server2
```

5. lvs本身没有应用服务器监控功能，如果web服务器出现问题，需要手工从规则中删掉

```shell
[root@web1 ~]# systemctl stop httpd
# 客户端访问时，转发到web1上的请求，将会拒绝连接
[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.15/; done
apache web server2
curl: (7) Failed connect to 192.168.4.15:80; 拒绝连接
apache web server2
apache web server2
curl: (7) Failed connect to 192.168.4.15:80; 拒绝连接
apache web server2
# 的规则中删除web1
[root@lvs1 ~]# ipvsadm -d -t 192.168.4.15:80 -r 192.168.4.100
[root@client1 ~]# for i in {1..6}; do curl http://192.168.4.15/; done
apache web server2
apache web server2
apache web server2
apache web server2
apache web server2
apache web server2
```

附：出错时，排错步骤：

```shell
# 在lvs上可以访问到web服务器
[root@lvs1 ~]# curl http://192.168.4.100/
192.168.2.100
[root@lvs1 ~]# curl http://192.168.4.200/
apache web server2

# 查看vip
[root@lvs1 ~]# ifconfig eth0:0
eth0:0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.4.15  netmask 255.255.255.0  broadcast 192.168.4.255
        ether 52:54:00:0d:fb:79  txqueuelen 1000  (Ethernet)

[root@web1 ~]# ifconfig lo:0
lo:0: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 192.168.4.15  netmask 255.255.255.255
        loop  txqueuelen 1000  (Local Loopback)

# 查看内核参数
[root@web1 ~]# sysctl -p
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.lo.arp_ignore = 1
net.ipv4.conf.all.arp_announce = 2
net.ipv4.conf.lo.arp_announce = 2

# 查看规则
[root@lvs1 ~]# ipvsadm -Ln
IP Virtual Server version 1.2.1 (size=4096)
Prot LocalAddress:Port Scheduler Flags
  -> RemoteAddress:Port           Forward Weight ActiveConn InActConn
TCP  192.168.4.15:80 wlc
  -> 192.168.4.100:80             Route   1      0          0         
  -> 192.168.4.200:80             Route   2      0          0  
```

